
#include <memory.h>
#include <stdlib.h>
#include <libcob.h>

#include <gtk/gtk.h>
#include <glib.h>
#include <unistd.h> 
#include <pthread.h> 

#define RENDER_COMPONENT_COUNT 200

struct RenderComponent {
    int x;
    int y;
    int height;
    int width;
} RenderComponent={0,0,0,0};

struct RenderComponent renderComponents[RENDER_COMPONENT_COUNT];
int direction=0;
char text[12] ="";
int dead = 0;

gboolean on_drawingarea_key_press_event(GtkWidget *widget, GdkEventKey *event,
                                        gpointer user_data)
{
    switch (event->keyval)
  {
    case GDK_KEY_w:
      direction=3;
      break;
    case GDK_KEY_d:
      direction=0;
      break;  
    case GDK_KEY_s:
      direction=1;
      break;
    case GDK_KEY_a:
      direction=2;
      break;
  }
	return FALSE;
}


void addRenderComponent(int x, int y, int height, int width){
    for(int i = 0; i<RENDER_COMPONENT_COUNT-1; i++){
        if(renderComponents[i].height==0){
            struct RenderComponent tempRenderComponent;
            tempRenderComponent.x=x;
            tempRenderComponent.y=y;
            tempRenderComponent.height=height;
            tempRenderComponent.width=width;
            renderComponents[i]=tempRenderComponent;
            break;
        }
    }
}


/* Initialize the toolkit, abends if not possible */
int
CBL_OC_GTK_INIT(int argc, char *argv[])
{
    gtk_init(&argc, &argv);
    return 0;
}



/* Initialize the toolkit, return false if not possible */
/* Need pointers to argc and argv here */
int
CBL_OC_GTK_INIT_CHECK()
{
    gboolean gres = gtk_init_check(0, NULL);
    return (gres == TRUE) ? 0 : -1;
}

int
INCREASE_TEST_INTEGER(int test)
{
    return test+1;
}


/* Create new window */
GtkWidget*
CBL_OC_GTK_WINDOW_NEW()
{   
    return gtk_window_new(GTK_WINDOW_TOPLEVEL);
}

/* set the title */
int
CBL_OC_GTK_WINDOW_SET_TITLE(void *window, char *title)
{   

    g_signal_connect ((gpointer) window, "key_press_event",
                    G_CALLBACK (on_drawingarea_key_press_event),
                    NULL);
    gtk_window_set_title(GTK_WINDOW(window), title);
    return 0;
}

/* Widget sizing */
int
CBL_OC_GTK_WIDGET_SET_SIZE_REQUEST(void *widget, int x, int y)
{
    gtk_widget_set_size_request(GTK_WIDGET(widget), x, y);
    return 0;
}

/* Set border width */
int
CBL_OC_GTK_CONTAINER_SET_BORDER_WIDTH(void *window, int pixels)
{
    gtk_container_set_border_width(GTK_CONTAINER(window), pixels);
    return 0;
}

/* New vertical box */
GtkWidget*
CBL_OC_GTK_VBOX_NEW(int homogeneous, int spacing)
{
    return gtk_vbox_new((gboolean)homogeneous, (gint)spacing);
}

/* New horizontal box */
GtkWidget*
CBL_OC_GTK_HBOX_NEW(int homogeneous, int spacing)
{
    return gtk_hbox_new((gboolean)homogeneous, (gint)spacing);
}

/* packing boxes */
int
CBL_OC_GTK_BOX_PACK_START(void *gcont, void *gobj, int expand,
                          int fill, int padding)
{
    gtk_box_pack_start(GTK_BOX(gcont), gobj, (gboolean)expand,
                       (gboolean)fill, (guint)padding);
    return 0;
}

gboolean
draw_callback (GtkWidget *widget, cairo_t *cr, gpointer data)
{
  GdkRGBA color;

  if(dead){
        gtk_style_context_get_color (gtk_widget_get_style_context (widget),
                               0,
                               &color);
    gdk_cairo_set_source_rgba (cr, &color);

    char deathText[40];
    strcpy(deathText, "You died! ");
    strcat(deathText, text);
    cairo_move_to(cr, 230, 250);
    cairo_show_text(cr, deathText);
    return FALSE;
  }

  for(int i =0; i<RENDER_COMPONENT_COUNT-1; i++){

    struct RenderComponent currentComponent = renderComponents[i];
    if(currentComponent.height==0){
        break;
    }
    cairo_rectangle(cr, currentComponent.x, currentComponent.y, currentComponent.width, currentComponent.height);
    gtk_style_context_get_color (gtk_widget_get_style_context (widget),
                               0,
                               &color);
    gdk_cairo_set_source_rgba (cr, &color);
    cairo_fill (cr);
  }
 cairo_move_to(cr, 10, 10);

 cairo_show_text(cr, text);

 return FALSE;
}

void
REDRAW(GtkWidget *widget){
    gtk_widget_queue_draw(widget);
}

GtkWidget*
CREATE_DEFAULT_CANVAS()
{
    GtkWidget *canvas = gtk_drawing_area_new ();
    gtk_widget_set_size_request (canvas, 500, 500);
      g_signal_connect (G_OBJECT (canvas), "draw",
                    G_CALLBACK (draw_callback), NULL);
    return canvas;
}

/* connect event to callback */
int
CBL_OC_G_SIGNAL_CONNECT(int *gobj, char *sgn, void (cb)(void *, void *), void *parm)
{
    g_signal_connect(G_OBJECT(gobj), sgn, G_CALLBACK(cb), parm);
    return 0;
}

/* add object to container */
int
CBL_OC_GTK_CONTAINER_ADD(void *window, void *gobj)
{
    gtk_container_add(GTK_CONTAINER(window), gobj);
    return 0;
}

/* tell gtk that object is now ready */
int
CBL_OC_GTK_WIDGET_SHOW(void *gobj)
{
    gtk_widget_show(gobj);
    return 0;
}

/* tell gtk to ready all the wdigets */
int
CBL_OC_GTK_WIDGET_SHOW_ALL(void *window)
{
    gtk_widget_show_all(window);
    return 0;
}


extern int MAINLOOP(GtkWidget *canvas);

void *thread(void *canvas) 
{ 
    cob_init(0, NULL);
    int ret;
    ret = MAINLOOP(canvas);    
    return NULL; 
}

void CSLEEP(){
    sleep(1);
}

int GET_LAST_KEYPRESS_CODE(){
    return direction;
}

void setScore(int score){
    int length = snprintf( NULL, 0, "Score: %d", score );
    snprintf(text, length+1, "Score: %d", score);
}

void SET_IS_SNAKE_DEAD(){
    dead=1;
}

void CLEAR_RENDER_COMPONENTS(){
  for(int i = 0; i<RENDER_COMPONENT_COUNT-1; i++){
        if(renderComponents[i].height==0){
            break;
        }
      struct RenderComponent tempRenderComponent;
            tempRenderComponent.x=0;
            tempRenderComponent.y=0;
            tempRenderComponent.height=0;
            tempRenderComponent.width=0;
            renderComponents[i]=tempRenderComponent;
    }
}

int
CBL_OC_GTK_MAIN(GtkWidget *canvas)
{   
    pthread_t thread_id; 

    gtk_widget_add_events(canvas,  GDK_KEY_PRESS_MASK);

    pthread_create(&thread_id, NULL, thread, canvas); 
    printf("UI thread created \n"); 
    gtk_main();

    return 0;
}   

int
CBL_OC_GTK_MAIN_QUIT()
{
    gtk_main_quit();
    return 0;
}
